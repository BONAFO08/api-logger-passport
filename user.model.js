
import Sequelize from 'sequelize';
import connection from "./config.database.js"



const User = connection.define(
    'users',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
        },
        nickname: {
            type: Sequelize.STRING,
        },
        name: {
            type: Sequelize.STRING,
        },
        password: {
            type: Sequelize.STRING,
        },
        githubId: {
            type: Sequelize.STRING,
        },
        email: {
            type: Sequelize.STRING,
        },
    },
    {
        timestamps: false,
    },
    {
        freezeTableName: true
    },

);


export default User;
