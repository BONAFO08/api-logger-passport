import 'dotenv/config.js'
import express from 'express';
import passport from 'passport'
import GitHubStrategy from 'passport-github2';
import Sequelize from 'sequelize';
import User from './user.model.js'
import path from 'path';


const app = express();
const port = 3000;



passport.use(new GitHubStrategy({
    clientID: "de5765e3a5c670a1cb2f",
    clientSecret: "bbec3f86fb9e5a18f7e22288a3df8eb880b69ca5",
    callbackURL: "https://mytokensito.loca.lt/auth/github/mytokensito"
},
    function (accessToken, refreshToken, profile, done) {
        User.findOrCreate(
            {
                where: {
                    githubId: profile.id,
                    nickname: profile.username,
                    name: profile.username,
                    password: profile.username
                }
            }).then((err, user) => {
                return done(err, user);
            }).catch(err => { })
    }
));

app.use(express.static(process.env.DIR));


app.get('/', (req, res) => {
    res.sendFile(path.join(process.env.DIR + '/index.html'))
})

app.get('/auth/github',
    passport.authenticate('github', { scope: ['user:email'] }));


app.get('/auth/github/mytokensito',
    passport.authenticate('github', { failureRedirect: '/login' }),
    function (req, res) {
        console.log('123');
        // Successful authentication, redirect home.

    });





app.listen(port, () => {
    console.log(`Server on in port ${port}`);
});

